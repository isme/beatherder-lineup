import lxml.html
import csv
import sys


def extract_artist_info(markup):
    doc = lxml.html.fromstring(markup)

    info = {
      'artist': doc.cssselect('h1.title')[0].text,
    }

    social_links = doc.xpath(
        '//div[contains(@class, "block--social") '
              'and .//text()[contains(.,"Find out more")]]//a')

    for a in social_links:
        key = a.get('class').replace('icon--', '')
        val = a.get('href')
        info[key] = val
    
    video = doc.cssselect('div.block--video iframe')

    if video:
        info['video'] = video[0].get('src')
    
    return info


def open_and_extract(fname):
    with (open(fname)) as f:
        markup = f.read()
        return extract_artist_info(markup)


if __name__ == '__main__':
    inputfiles = sys.argv[1:]
    infos = [open_and_extract(f) for f in inputfiles]

    fieldnames = set()
    for i in infos:
      fieldnames.update(i.keys())
    
    csvout = csv.DictWriter(sys.stdout, fieldnames=fieldnames)
    csvout.writeheader()
    csvout.writerows(infos)



