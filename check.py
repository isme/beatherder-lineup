import lxml.html
import sys

if __name__ == '__main__':

    classes = set()

    for f in sys.argv[1:]:
      markup = open(f).read()
      doc = lxml.html.fromstring(markup)
      classes |= set(doc.xpath('//div[contains(@class, "block--social") and .//text()[contains(.,"Find out more")]]//a/@class'))

    print(classes)
