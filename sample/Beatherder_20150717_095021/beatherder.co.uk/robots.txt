<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">    
    <!--[if lt IE 9]><script src='/public/scripts/libs/html5shiv.min.js'></script><![endif]-->

    <title>Beat-Herder Festival (17th &ndash; 19th July 2015.) 3 more days of beats and barminess, The Ribble Valley, Lancashire</title>

    <meta property="og:title" content="The Beat-Herder Festival - 17<sup>th</sup> &ndash; 19<sup>th</sup> July 2015." />
    <meta property="og:description" content="3 more days of beats and barminess, The Ribble Valley, Lancashire" />
    <meta property="og:image" content="http://beatherder.co.uk/img/facebook.png" />
    
    <link href='/public/styles/master.css' rel='stylesheet' />
    <link href='http://fonts.googleapis.com/css?family=Arbutus+Slab|Oswald:400,300' rel='stylesheet' type='text/css'>
    
    <link href="/public/images/favicon.ico" rel="shortcut icon">
</head>

<body>

    <div class="wrapper nav-active">
        <header class="header">
            <div class="container">
                <a href="/" class="header__logo">The Beat-Herder Festival</a>

                <p class="header__dates">
                    17<sup>th</sup> &ndash; 19<sup>th</sup> July 2015., The Ribble Valley, Lovely Lancashire.
                </p>

                <p class="header__disc">
                    Herm ’em Up
                </p>

                                 
                    <a href="http://tickets.beatherder.co.uk/" class="header__tickets">
                        <p class="header__tickets-label">
                            <span class="big">Buy</span> Tickets
                        </p>

                        <p class="header__tickets-btn btn">Click Here</p>
                    </a>

                
                <div class="header__social">
                    <p class="header__social-label">Follow Us</p>

                    <ul class="nav nav--banner">
                        <li><a href="http://www.facebook.com/BeatHerder" class="icon--facebook"><span class="hide-text">Facebook</span></a></li>
                        <li><a href="http://www.twitter.com/beatherder" class="icon--twitter"><span class="hide-text">Twitter</span></a></li>
                        <li><a href="http://www.flickr.com/photos/tags/beatherder/" class="icon--flickr"><span class="hide-text">Flickr</span></a></li>
                        <li><a href="http://www.youtube.com/user/beatherder" class="icon--youtube"><span class="hide-text">YouTube</span></a></li>
                    </ul>
                </div>

                <a href="#" class="header__trigger js-menuTrigger icon--menu"></a>
            </div>

            <nav class="header__nav">
                <ul class="nav nav--banner">
                    <li class=""><a href="/" class="header__nav-link">Home</a></li>
                    <li class=""><a href="http://tickets.beatherder.co.uk/" class="header__nav-link">Tickets</a></li>
                    <li class=""><a href="/line-up" class="header__nav-link">Line Up</a></li>
                    <li class=""><a href="/glamping" class="header__nav-link">Glamping</a></li>
                    <li class=""><a href="/faq" class="header__nav-link">FAQ</a></li>
                    <li class=""><a href="/be-involved" class="header__nav-link">Be Involved</a></li>
                </ul>
            </nav>
        </header>

        <div class="container">



		<div class="title-container">
			<h1 class="title">Damn! Something is missing.</h2>
		</div>

		<div class="block block--text">
			<p>That page you were looking for may have been moved, updated or deleted. Sorry about that.</p><p>Maybe it’s time we took you <a href="/">home</a>.</p>
		</div>            <section class="contact">
                <div class="tablet--half block block--social contact__social">
                    <h3 class="block__heading">Follow Us</h3>
                    <ul class="nav nav--banner">
                        <li><a href="http://www.facebook.com/BeatHerder" class="icon--facebook"><span class="hide-text">Facebook</span></a></li>
                        <li><a href="http://www.twitter.com/beatherder" class="icon--twitter"><span class="hide-text">Twitter</span></a></li>
                        <li><a href="http://www.flickr.com/photos/tags/beatherder/" class="icon--flickr"><span class="hide-text">Flickr</span></a></li>
                        <li><a href="http://www.youtube.com/user/beatherder" class="icon--youtube"><span class="hide-text">YouTube</span></a></li>
                    </ul>
                </div>

                <div class="tablet--half block contact__newsletter">
                    <h3 class="block__heading">Join Our Mailing List</h3>

                    <form action="http://news.beatherder.co.uk/t/y/s/idludd/" method="post" >
                        <ul class="form-fields">
                            <li>
                                <label for="name">Name:</label>
                                <input type="text" id="name" name="cm-name"  />
                            </li>
                            <li>
                                <label for="idludd-idludd">Email:</label>
                                <input name="cm-idludd-idludd" id="idludd-idludd" type="email" />
                            </li>
                            <li>
                                <input type="submit" value="Subscribe" class="btn btn--small" />
                            </li>
                        </ul>
                    </form>
                </div>
            </section>

        </div><!-- .container -->

        <footer class="footer">
            <div class="container">
                <nav class="footer__nav">
                    <ul class="nav">
                        <li class="footer__nav-item"><a href="/faq">FAQ</a></li>
                        <li class="footer__nav-item"><a href="/legalities">Terms &amp; Conditions</a></li>
                        <li class="footer__nav-item"><a href="/map">Map</a></li>
                        <li class="footer__nav-item"><a href="/be-involved">Contact</a></li>
                    </ul>
                </nav>

                <nav class="footer__credits">
                    <ul class="nav">
                        <li class="footer__credits-item">Design by: <a href="http://jonsimmons.prosite.com/" class="footer__credits--jon-simmons">Jon Simmons</a></li>
                        <li class="footer__credits-item">Website by: <a href="http://designition.co.uk" class="footer__credits--designition">Designition</a></li>
                    </ul>
                </nav>

                <div class="footer__image">
                </div>
            </div>
        </footer>

    </div><!-- .wrapper -->




    <div class="empty-div"></div>

            <script type="text/javascript">
        var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
        document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
        </script>
        <script type="text/javascript">
        try {
        var pageTracker = _gat._getTracker("UA-2774842-24");
        pageTracker._trackPageview();
        } catch(err) {}</script>
        

    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="/public/scripts/libs/jquery-1.10.2.min.js"><\/script>')</script>
    <script src="/public/scripts/libs/responsiveSlides.min.js"></script>
    <script src="/public/scripts/master.js"></script>


</body>
</html>