export OUTDIR=Beatherder_$(date --utc "+%Y%m%d_%H%M%S")


wget \
  --recursive \
  --accept-regex "artists" \
  --directory-prefix $OUTDIR \
  "http://beatherder.co.uk/line-up"


python3 extract.py $OUTDIR/beatherder.co.uk/artists/* > lineup.csv

